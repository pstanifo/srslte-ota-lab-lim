#!/usr/bin/env bash

# To be run on the `nodeb` compute node in the "scenario 2" profile.
# This adds a route back to the RAN subnet (172.16.0.0/24) via the EPC

MYPATH=$(realpath $0 | xargs dirname)
RAN_SUBNET='172.16.0.0/24'
EPC_IPADDR=$(getent hosts epc | cut -s -d ' ' -f 1)
if [[ $EPC_IPADDR == "" ]]
then
  # If there is no epc node, use the enb1 node instead
  EPC_IPADDR=$(getent hosts enb1 | cut -s -d ' ' -f 1)
fi

ERR=0

sudo ip route add $RAN_SUBNET via $EPC_IPADDR || ERR=1

if [[ $ERR == 1 ]]; then
	echo $(basename $0) "Failed to add routes!"
else
	echo $(basename $0) "Finished adding routes."
fi | $MYPATH/log.sh

exit $ERR
