#!/usr/bin/env python
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.rspec.emulab.spectrum as spectrum
import geni.urn as URN


tourDescription = """

## srsLTE with COTS UE lab environment OTA

Use this profile to intantiate an end-to-end LTE network with over-the-air lab
resources (short, line of site links between UEs and eNB). The following
resources are requested:

* 3 Pixel XL phones (`rue1`,`rue2`,`rue3`)
* An Intel NUC5300/B210 w/ srsLTE eNB/EPC (`enb1`)

For the "ADB target address" parameter, specify the Internet-facing
hostname or IP address of a machine under your control that has
Android ADB tools installed.  This could even be a compute node in a
different experiment on POWDER (use its "oontrol network" IP address
in this case).

"""

tourInstructions = """

### Start EPC and eNB

After your experiment becomes ready, login to `enb1` via `ssh` and do:

```
/local/repository/bin/start.sh
```

This will start a `tmux` session with three panes, running `srsepc` and
`srsenb`, and then leaving your cursor in the last pane. After you've associated
a UE with this eNB, you can use the third pane to run tests with `ping` or
`iperf`. If you are not familiar with `tmux`, it's a terminal multiplexer that
has some similarities to screen. Here's a [tmux cheat
sheet](https://tmuxcheatsheet.com), but `ctrl-b o` (move to other pane) and
`ctrl-b x` (kill pane), should get you pretty far. `ctrl-b d` will detach you
from the `tmux` session and leave it running in the background. You can reattach
with `tmux attach`.

If you'd like to start `srsepc` and `srsenb` manually, here are the commands:

```
# start srsepc
sudo srsepc /etc/srslte/epc.conf

# start srsenb
sudo srsenb /etc/srslte/enb.conf
```

### Connect to COTS phone via ADB

You should see the COTS phone sync with the eNB eventually and obtain
an IP address. Look at the "manifest" tab of your experiment to get
the name of the host and port to connect to.  The XML element for the
`rue1` node will have an `adb_port` attribute.  Make a note of this
port.  There will also be a "emulab:console_server" sub-element for
the `rue1` node.  Find this and note the host name listed.  Then, on
the ADB target host you specified in the parameters when
instantiating, do the following to connect to the UE:

```
adb connect <console_server>:<adb_port>
adb shell
```

Note: it may take up to 5 minutes after the experiment becomes ready for `adb
connect` to succeed, so if it doesn't work the first time you try, just wait a
few minutes and try again.

Once you have an `adb` shell to any of the UEs, you can use `ping` to test the
connection, e.g.,

```
# in adb shell connected to rue1
# ping SGi IP
ping 172.16.0.1
```

If a UE fails to sync with the eNB, try rebooting it via the `adb` shell. After
reboot, you'll have to repeat the `adb connect` and `adb shell` commands to
reestablish a connection to the phone. In some cases, toggling Airplane mode
will also encourage the phones to sync. You can accomplish this via the `adb`
shell by doing:

```
# in adb shell
# toggle airplane mode
su
settings put global airplane_mode_on 1
am broadcast -a android.intent.action.AIRPLANE_MODE
sleep 2
settings put global airplane_mode_on 0
am broadcast -a android.intent.action.AIRPLANE_MODE
```

"""


class GLOBALS(object):
    NUC_HWTYPE = "nuc5300"
    COTS_UE_HWTYPE = "pixel3a-PL"
    DLHIFREQ = 2690.0
    DLLOFREQ = 2680.0
    ULHIFREQ = 2570.0
    ULLOFREQ = 2560.0
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:U18LL-SRSLTE:0"
    PE_AGG = "urn:publicid:IDN+pe-test1.powderwireless.net+authority+cm"
    LAB_EPC_NODE = "nuc12"
    LAB_ADB_NODE = "nuc1"

pc = portal.Context()

# Define profile parameters
pc.defineParameter(
    "num_ues",
    "Number of phones.",
    portal.ParameterType.INTEGER, 3, [1,2,3],
    "The selected number of phones to be used"
)

portal.context.defineParameter(
    "cnodetype",
    "Compute node type",
    portal.ParameterType.STRING, "d430",
    ["","d710","d430","d740","none"],
    "Type of compute nodes attached to the eNB/core and UE nodes running NDN software."
)

pc.defineParameter("adb_tgt", "ADB target address",
                   portal.ParameterType.STRING, "",
                   longDescription="IP address or hostname of the (remote) host you will use to connect to the UE using ADB.")

pc.defineParameter("dlspechi", "Downlink range high frequency",
                   portal.ParameterType.BANDWIDTH, GLOBALS.DLHIFREQ)

pc.defineParameter("dlspeclo", "Downlink range low frequency",
                   portal.ParameterType.BANDWIDTH, GLOBALS.DLLOFREQ)

pc.defineParameter("ulspechi", "Uplink range high frequency",
                   portal.ParameterType.BANDWIDTH, GLOBALS.ULHIFREQ)

pc.defineParameter("ulspeclo", "Uplink range low frequency",
                   portal.ParameterType.BANDWIDTH, GLOBALS.ULLOFREQ)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

# Add a NUC eNB node
enb1 = request.RawPC("enb1")
enb1.component_id = GLOBALS.LAB_EPC_NODE
enb1.hardware_type = GLOBALS.NUC_HWTYPE
enb1.disk_image = GLOBALS.SRSLTE_IMG
enb1.Desire("rf-radiated", 1)
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
enb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))

for id in range(1, params.num_ues + 1):
    # Add UE (phone).
    rue = request.UE("rue%d" % id)
    rue.component_manager_id = GLOBALS.PE_AGG
    rue.hardware_type = GLOBALS.COTS_UE_HWTYPE
    rue.setUseTypeDefaultImage()
    rue.adb_target = params.adb_tgt

# Add ADB server and NDN server, core network side
if params.cnodetype != "none":
    # Add ADB node
    adb = request.RawPC("adb")
    adb.component_manager_id = GLOBALS.PE_AGG
    adb.component_id = GLOBALS.LAB_ADB_NODE
    adb.hardware_type = params.cnodetype
    adb.disk_image = GLOBALS.UBUNTU_1804_IMG
    adb.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh"))

    cndn = request.RawPC("cndn")
    cndn.hardware_type = params.cnodetype
    cndn.disk_image = GLOBALS.UBUNTU_1804_IMG
    cndn_link = request.Link("cndn-link", members=[cndn, enb1])
    #adb_link = request.Link("adb-link", members=[cndn, adb])
    cndn.addService(rspec.Execute(shell="bash", command="/local/repository/routes.sh"))
    cndn.addService(rspec.Execute(shell="bash", command="/local/repository/install.sh"))

    # We need a link to talk to the remote file system, so make an interface.
    cndn_iface = cndn.addInterface()

    # The remote file system is represented by special node.
    cndn_fsnode = request.RemoteBlockstore("cndn-fsnode", "/mydata")
    # This URN is displayed in the web interfaace for your dataset.
    # VM Dataset
    cndn_fsnode.dataset = "urn:publicid:IDN+emulab.net:verinet-5g+ltdataset+nfd-gw"
    #
    # The "rwclone" attribute allows you to map a writable copy of the
    # indicated SAN-based dataset. In this way, multiple nodes can map
    # the same dataset simultaneously. In many situations, this is more
    # useful than a "readonly" mapping. For example, a dataset
    # containing a Linux source tree could be mapped into multiple
    # nodes, each of which could do its own independent,
    # non-conflicting configure and build in their respective copies.
    # Currently, rwclones are "ephemeral" in that any changes made are
    # lost when the experiment mapping the clone is terminated.
    #
    cndn_fsnode.rwclone = True

    #
    # The "readonly" attribute, like the rwclone attribute, allows you to
    # map a dataset onto multiple nodes simultaneously. But with readonly,
    # those mappings will only allow read access (duh!) and any filesystem
    # (/mydata in this example) will thus be mounted read-only. Currently,
    # readonly mappings are implemented as clones that are exported
    # allowing just read access, so there are minimal efficiency reasons to
    # use a readonly mapping rather than a clone. The main reason to use a
    # readonly mapping is to avoid a situation in which you forget that
    # changes to a clone dataset are ephemeral, and then lose some
    # important changes when you terminate the experiment.
    #
    #cndn_fsnode.readonly = True

    # Now we add the link between the node and the special node
    cndn_fslink = request.Link("cndn-fslink")
    cndn_fslink.addInterface(cndn_iface)
    cndn_fslink.addInterface(cndn_fsnode.interface)

    # Special attributes for this link that we must use.
    cndn_fslink.best_effort = True
    cndn_fslink.vlan_tagging = True

#request.requestSpectrum(params.dlspeclo, params.dlspechi, 100)
#request.requestSpectrum(params.ulspeclo, params.ulspechi, 100)

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
