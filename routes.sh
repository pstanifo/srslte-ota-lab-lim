#!/usr/bin/env bash

# Run appropriate route script

MYPATH=$(realpath $0 | xargs dirname)
NODE_ID=$(geni-get client_id)

if [[ $NODE_ID == "cndn" ]]; then
    SCRIPT=$MYPATH/routes-cndn.sh
else
    echo "no routes to add on $NODE_ID"
    exit 0
fi

exec sudo $SCRIPT
