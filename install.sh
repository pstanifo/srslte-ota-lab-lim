#!/bin/bash

# Run appropriate install scripts

MYPATH=$(realpath $0 | xargs dirname)
INSTALL_PATH="${MYPATH}/install-scripts/"
DATASET_PATH="/mydata/configure-scripts/"
NODE_ID=$(geni-get client_id)
VM_NUMBER=$1

${INSTALL_PATH}/update-apt-get.sh

if [[ $NODE_ID == "sue"* ]]; then
    ${MYPATH}/add-vm-bridge.sh
    ${INSTALL_PATH}/install-kvm.sh
    ${INSTALL_PATH}/install-udisks2.sh
    ${DATASET_PATH}/configure.sh ${VM_NUMBER} 2>&1 > /tmp/install.log
elif [[ $NODE_ID == "cndn" ]]; then
    ${INSTALL_PATH}/install-runc.sh
    ${DATASET_PATH}/configure.sh 2>&1 > /tmp/install.log
elif [[ $NODE_ID == "adb" ]]; then
    ${INSTALL_PATH}/install-adb.sh
else
    echo "No packages to install on $NODE_ID"
    exit 0
fi
